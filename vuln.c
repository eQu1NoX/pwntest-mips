#include<stdio.h>

void ret_15(void) {
  asm("li $v0,4119\n");
  asm("syscall\n");
  asm("jr $ra\n");
}

int read_input() {
  char buffer[512];
  printf("Buffer = %p\n", buffer);
  printf("Function = %p\n", ret_15);
  read(0, buffer, 1000);
  return 0;
}

int main() {
read_input();
return 0;
}
