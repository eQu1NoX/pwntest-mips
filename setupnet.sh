
echo "[+] Bringing up tap0"
sudo tunctl -t tap0 && sudo ifconfig tap0 up
echo "[+] Adding tap0 to virbr0"
sudo brctl addif virbr0 tap0
echo "[+] Adding eth1 to virbr0"
sudo brctl addif virbr0 eth1

echo "[+] Removing ip from eth1"
sudo ip addr del 10.0.3.15/24 dev eth1

echo "[+] Adding ip to virbr0"
sudo ip addr del 192.168.122.1/24 dev virbr0
sudo ip addr add 10.0.3.15/24 dev virbr0


echo "--------------------------------"
ifconfig
echo "--------------------------------"
sudo brctl show virbr0
