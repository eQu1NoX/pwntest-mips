from pwn import *

def getoffset(value):
    return cyclic_find(value.decode("hex"))

regs = "zero       at       v0       v1       a0       a1       a2       a3 t0       t1       t2       t3       t4       t5       t6       t7 s0       s1       s2       s3       s4       s5       s6       s7 t8       t9       k0       k1       gp       sp       s8       ra status       lo       hi badvaddr    cause       pc".split()
vals = "00000000 6e616161 70616161 72616161 74616161 76616161 78616161 7a616162 63616162 65616162 67616162 69616162 6b616162 6d616162 6f616162 71616162 73616162 75616162 77616162 79616162 62616163 64616163 66616163 68616163 6a616163 6c616163 6e616163 70616163 72616163 74616163 76616163 78616163 0000a413 00000000 00000000 6a616160 10800008 6a616161".split()

final = []
for reg, val in zip(regs, vals):
    final.append((getoffset(val), reg))

final.sort()
x = ""
print len(final)
for each in final:
    x += '"JUNK", "%s", ' % each[1]

print x
